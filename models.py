from server import db
from sqlalchemy.dialects.postgresql import BYTEA, INTEGER, MONEY, TEXT, TIMESTAMP


class WareUnit(db.Model):

    __tablename__ = 'goods'

    id = db.Column(INTEGER, primary_key=True)
    title = db.Column(TEXT)
    price = db.Column(MONEY)
    about_info = db.Column(TEXT)
    pictures = db.Column(BYTEA)
    date_created = db.Column(TIMESTAMP)

    def __init__(self, title, price, about_info, pictures, date_created):
        self.title = title
        self.price = price
        self.about_info = about_info
        self.pictures = pictures
        self.date_created = date_created

    def __repr__(self):
        return f"{self.title} ; {self.price} - {self.date_created}"

