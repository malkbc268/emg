https://hub.docker.com/_/postgres

docker network create exhb

docker run --network exhb -d -p '127.0.0.1:5432:5432' --env-file .env    --name some-postgres  -v pg_data:/var/lib/postgresql/data     postgres

docker run --network exhb -d -p '127.0.0.1:5000:5000' --env-file .env --name exhb exhibition:latest