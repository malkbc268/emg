import os
import json
from pytz import timezone
from base64 import b64encode
from datetime import datetime
from flask import Flask, request, send_file
from flask_cors import cross_origin
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from pdb import set_trace

tz = timezone('Europe/Kiev')
app = Flask(__name__)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

app.config['DEBUG'] = True
app.config['TESTING'] = True
app.config['CSRF_ENABLED'] = True
app.config['SECRET_KEY'] = 'this-really-needs-to-be-changed'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI')
MAX_CONTENT_LENGTH = 20 * 1024 * 1024
SERVER_URL = os.getenv('SERVER_URL')
app.config['MAX_CONTENT_LENGTH'] = MAX_CONTENT_LENGTH

from models import WareUnit


@app.route('/')
def home():
    return send_file('index.html')

@app.route('/all.json')
# @cross_origin()
def index():
    goods_units = WareUnit.query.all()
    goods_list = [
        {
            'title': ware.title,
            'about_info': ware.about_info,
            'date_created': ware.date_created.isoformat(),
            'price': ware.price,
            'pictures': ware.pictures.decode('utf-8').split('....') # zip data:image/jpg;base64,'
        } for ware in goods_units
    ]
    return json.dumps(goods_list)

@app.route('/save_picture', methods=['POST'])
def upload_imago():
    figures_list = bytes(
        'data:image/jpg;base64,' + '....data:image/jpg;base64,'.join(
        [
            b64encode(request.files[img].read(MAX_CONTENT_LENGTH)).decode('utf-8') for img in request.files
        ]
    ), 'utf-8')
    new_goods_entry = WareUnit(
        title=request.form['title'],
        price=request.form['price'],
        about_info=request.form['about_info'],
        date_created=datetime.now(tz=tz).isoformat(),
        pictures=figures_list,
    )
    db.session.add(new_goods_entry)
    db.session.commit()
    return 'CREATED', 201

if __name__ == '__main__':
    app.run()

