
FROM python:3.6.8-slim

ENV TZ=Europe/Kiev
ENV FLASK_ENV=development
ENV FLASK_APP=server.py
RUN groupadd -r exhibition && useradd -s /bin/bash -m -d /home/exhibition -g exhibition exhibition
RUN apt-get update && apt-get install -y python3-pip vim curl gettext-base
WORKDIR /home/exhibition
ADD requirements.txt requirements.txt
RUN python3 -m pip install -r requirements.txt
ADD index.html /home/exhibition/template.html
ADD . /home/exhibition/
RUN chown -R exhibition:exhibition /home/exhibition

USER exhibition
CMD /bin/bash -c "envsubst < /home/exhibition/template.html | tee index.html; flask db init; flask db migrate; flask db upgrade; flask run -h 0.0.0.0 -p 5000"

